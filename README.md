# Vue.JS Sandbox App
This is just an app for playing around with Vue.js. The intent is to grow it to multiple little samples over time (hence the wonky file structure). Currently it's just one project.  

I'm trying to set some guidelines per project. For now they look like this: 
1) Short timeline to completion: 1-2 weeks
2) As much custom code as possible (unless the goal is to play with a specific library). Basically avoiding cut and paste code, or lots of npm modules. 
3) Ignore practical applications in favor of future looking code. (I don't care if this works in IE 10 or less). 
4) Make it work OK on mobile.

You can view the current progress here:
https://myklb.gitlab.io/vuejs-sandbox/

## Project setup
```
1) clone git@gitlab.com:myklb/vuejs-sandbox
2) npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Lints and fixes files
```
npm run lint
```
