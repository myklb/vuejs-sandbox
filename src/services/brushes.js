export default function brushes(coordinateArray, brushType, variation = 1) {

  this.data = [];
  
  this.generateBrushes = (coordinates, brushType, variation) => {
    const brushes = [];
    for (let i = 0; i < coordinates.length; i++) {
      const s = randomizeSize();
      const randomizedVariation = Math.ceil(Math.random() * variation);
      brushes.push(
        {
          posX: parseInt((coordinates[i].x - s / 2).toFixed()),
          posY: parseInt((coordinates[i].y - s / 2).toFixed()),
          size: s,
          opac: randomizeOpacity(),
          type: brushType,
          variation: randomizedVariation
        }
      )
    }
    this.data = brushes;
  }


  //for brushes that fill big areas, remove bushes that overlap
  //too much so we aren't 
  this.removeOverlaps = (OverlapPercent) => { 
    const bData = [...this.data];
    bData.map(brush => {
      const deleteIndexes = [];
      const boundSize = brush.size/2 * OverlapPercent ;
      const cX = brush.posX;
      const cY = brush.posY;
      const bounds = {
        topL: {
          x: cX - boundSize,
          y: cY - boundSize
        },
        bottomR: {
          x: cX + boundSize,
          y: cY + boundSize
        }
      }
      
      bData.map((brsh, i) => {
        if ((brsh.posX > bounds.topL.x && brsh.posY > bounds.topL.y)
          && (brsh.posX < bounds.bottomR.x && brsh.posY < bounds.bottomR.y)) {
          deleteIndexes.push(i);
        }
      });

      if(deleteIndexes.length > 0 ){
        deleteIndexes.map(arrayIndex => {
          bData.splice(arrayIndex,1)
        });
      }
    });
    this.data = bData;
    return bData;
  }
  
  //private
  const randomizeSize = () => {
    const midpoint = 250;
    const variation = 50;
    return calculateRandomInRange(midpoint, variation)
  }

  const randomizeOpacity = () => {
    const opacity = calculateRandomInRange(70,25)/100;
    return opacity
  }
  
  const calculateRandomInRange = (midpoint, variation) => { 
    const min = midpoint - variation;
    const max = midpoint + variation;
    const result =((Math.random() * (max - min)) + min).toFixed(0);
    return result
  }

  //initialize
  this.generateBrushes(coordinateArray, brushType, variation);
}