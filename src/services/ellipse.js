export default function ellipse(center, radius, theta) {
  // properties
  this.center = { ...center };
  this.radius = { ...radius };
  this.theta = theta || 0;


  // private methods
  const randomizePoint = (point,spread) => {
    let s = spread;
    let randPoint = randInRange(point, s);
    return randPoint
  }

  const randInRange = (point, range) => { 
    const r = range ? range : 0;
    const p = point;
    let randPoint = Math.floor((Math.random() * r) + (p - r/2));
    return randPoint
  }

  const calculateCoordinate = (xRadius, yRadius) => {
    const coordinates = {
        x: xRadius * Math.cos(this.theta) - yRadius * Math.sin(this.theta) + this.center.x,
        y: xRadius * Math.sin(this.theta) + yRadius * Math.cos(this.theta) + this.center.y
    }
    return coordinates
  }

  const randomizeCoordinate = (coord) => {
    const newCoord = {
      x: randomizePoint(coord.x, 100),
      y: randomizePoint(coord.y, 100)
    }
    return newCoord
  }

  const randArray = (length) => {
    const matrix = [];
    for (let i = 0; i < length; i++){
      matrix.push(Math.random() * .8);
    }
    return matrix
  }

  
  //public methods
  this.randomizedCircumfrancePoints = (density, relativeSize) => { 
    const coordinates = [];
    const size = relativeSize || 0;    
    //make some new coordinates
    for (let i = 0; i < density; i++) { 
      let t = Math.random()*(2*Math.PI);
      let xR = (this.radius.x + size) * (Math.cos(t));
      let yR = (this.radius.y + size) * (Math.sin(t));
      let coordinate = calculateCoordinate(xR, yR);
      coordinate = randomizeCoordinate(coordinate);
      coordinates.push(coordinate);
    }
    return coordinates
  }

  this.randomizedFillPoints = (density, relativeSize) => { 
    const coordinates = [];    
    const size = relativeSize || 0;
    const randomArray = randArray(density);
    
    //make some new coordinates
    for (let i = 0; i < randomArray.length; i++){
      let t = Math.random()*(2*Math.PI);
      let s = Math.sqrt(randomArray[i]);
      let xR = (this.radius.x + size) * s * (Math.cos(t));
      let yR = (this.radius.y + size) * s * (Math.sin(t));
      coordinates.push(calculateCoordinate(xR,yR));
    }
    return coordinates
  }
}
